import { DateService } from './../shared/date.service';
import { Component, OnInit  } from '@angular/core';
import * as moment from 'moment';

interface Day {
  value: moment.Moment
  //heutige Tag
  active: boolean
  disabled: boolean
  //Tag, den wir ausgewählt haben
  selected: boolean
}

interface Week {
  days: Day[]
}

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss']
})
export class SelectorComponent implements OnInit{

//schließen Date Service from shared
//können direkt in den template eintragen
  constructor(public dateService: DateService) { }

go(dir: number){
  this.dateService.changeMonth(dir)
}
calendar: Week[]

 

  ngOnInit(): void {
    this.dateService.date.subscribe(this.generate.bind(this))
  }

  generate(now: moment.Moment){
    const startDay = now.clone().startOf('month').startOf('week')
    //Gränzen für Kalendar
    const endDay = now.clone().endOf('month').endOf('week')

    const date = startDay.clone().subtract(1, 'day')

    const calendar = []

    while(date.isBefore(endDay, 'day')){
      calendar.push({
        days: Array(7)
        .fill(0)
        .map(() => {
          const value = date.add(1, 'day').clone()
          const active = moment().isSame(value, 'date')
          //wenn der Monat mit den heutigen nicht übereinstimmt, dann disabled 
          const disabled = !now.isSame(value, 'month')

          //in now wir speichern heutigen Tag
          const selected = now.isSame(value, 'date')

          return {
            value, active, disabled, selected
          }
        })
      })
    }
this.calendar = calendar
  }
select(day: moment.Moment){
  this.dateService.changeDate(day)
}

}
