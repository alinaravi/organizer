
import { SelectorComponent } from './selector/selector.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ToDoListComponent } from './to-do-list/to-do-list.component';


const routes: Routes = [

 {path: '', component: SelectorComponent},
 {path: 'todo', component: ToDoListComponent}
]

@NgModule({
  exports: [RouterModule],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
